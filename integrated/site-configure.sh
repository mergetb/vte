#!/bin/bash

set -e

ansible-playbook \
    -i .rvn/ansible-hosts \
    -i ansible-interpreters \
    site/ansible/checksum-offload.yml

ansible-playbook \
    -i .rvn/ansible-hosts \
    -i ansible-interpreters \
    --extra-vars "sledTag=testing" \
    --extra-vars "@site/xir/tb-deploy-vars.json" \
    --extra-vars "xir=`pwd`/site/xir/spineleaf.mergetb.test.json" \
    --extra-vars "@image-sources.yml" \
    tb-deploy/provision.yml

ansible-playbook \
    -i .rvn/ansible-hosts \
    -i ansible-interpreters \
    devel.yml

ansible-playbook \
    -i .rvn/ansible-hosts \
    -i ansible-interpreters \
    --extra-vars "@site/xir/tb-deploy-vars.json" \
    --extra-vars "xir=`pwd`/site/xir/spineleaf.mergetb.test.json" \
    tb-deploy/init.yml
