#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "usage: multimat <n>"
  return
fi

n=$1

for i in `seq 0 $n`; do
  mergetb materialize hi one &
done

wait
