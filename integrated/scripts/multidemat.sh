#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "usage: multimat <n>"
  return
fi

n=$1

for i in `seq 0 $n`; do
  mergetb dematerialize hi one &
done

wait
