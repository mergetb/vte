#!/bin/bash

set -e
set -x

if [[ -z $user ]]; then
    echo "must set user"
    exit 1
fi

if [[ -z $passwd ]]; then
    echo "must set passwd"
    exit 1
fi

# run me in a container
if [[ ! $in_container ]]; then
    docker build -t test-runner -f test-runner.dock .
    docker run                                      \
        --net host                                  \
        --cap-add NET_ADMIN                         \
        --env user=$user                            \
        --env passwd=$passwd                        \
        --env px=`rvn ip proxy`                     \
        test-runner
    exit
fi

# perform local setup

source ./local-merge-setup.sh $user
ssh-keygen -t rsa -f ~/.ssh/id_rsa -N "" -q

mergetb pubkey ~/.ssh/id_rsa.pub

# XXX proxy jump does not work from shell script for some reason .... so using
# ProxyCommand
mkdir -p ~/.ssh
cat << EOF > ~/.ssh/config
Host *-$user
    Hostname %h
    User $user
    ProxyCommand ssh -o StrictHostKeyChecking=no -p 2202 -i ~/.ssh/id_rsa -W %h:%p $user@jumpc.mergetb.io -o UserKnownHostsFile=/dev/null
    StrictHostKeyChecking no
    UserKnownHostsFile=/dev/null
    IdentityFile ~/.ssh/id_rsa
EOF

# create experiment and launch XDC

pushd experiments/moa-mpl
mergetb delete exp mpl || true
mergetb new exp mpl --src=model.py
mergetb new xdc mpl falco
popd

sleep 5

./xdc-config.sh $user mpl falco

# copy experiment files to XDC and run experiment

set +e

scp -F ~/.ssh/config -r experiments falco-mpl-$user:
scp ~/.ssh/id_rsa falco-mpl-$user:.ssh/
scp ~/.ssh/id_rsa.pub falco-mpl-$user:.ssh/
ssh -F ~/.ssh/config falco-mpl-$user \
    "cd ~$user/experiments/moa-mpl && user=$user passwd=$passwd ./run.sh"

