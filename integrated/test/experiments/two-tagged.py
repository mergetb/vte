import mergexp as mx
from mergexp.net import capacity, latency
from mergexp.unit import mbps, ms

net = mx.Topology('two-tagged')
a = net.device('a')
b = net.device('b')
ab = net.connect([a, b], capacity == mbps(100), latency == ms(10))
ab.props['tags'] = ['wan', 'ab']

# access endpoint by link operator
ab[a].ip.addrs = ['10.0.0.1/24']
ab[b].ip.addrs = ['10.0.0.2/24']

# specify the net object as the experiment topology created by this script
mx.experiment(net)

