import mergexp as mx
from mergexp.net import capacity, latency
from mergexp.unit import mbps, ms

net = mx.Topology('moa-chain')

a = net.device('a')
b = net.device('b')
c = net.device('c')
d = net.device('d')
e = net.device('e')
f = net.device('f')
g = net.device('g')
h = net.device('h')

ab = net.connect([a, b], capacity == mbps(10), latency == ms(5))
ab[a].ip.addrs = ['10.0.0.1/24']
ab[b].ip.addrs = ['10.0.0.2/24']

bc = net.connect([b, c], capacity == mbps(100))
bc[b].ip.addrs = ['10.0.1.1/24']
bc[c].ip.addrs = ['10.0.1.2/24']

cd = net.connect([c, d], capacity == mbps(10), latency == ms(5))
cd[c].ip.addrs = ['10.0.2.1/24']
cd[d].ip.addrs = ['10.0.2.2/24']

de = net.connect([d, e], capacity == mbps(100))
de[d].ip.addrs = ['10.0.3.1/24']
de[e].ip.addrs = ['10.0.3.2/24']

ef = net.connect([e, f], capacity == mbps(10), latency == ms(5))
ef[e].ip.addrs = ['10.0.4.1/24']
ef[f].ip.addrs = ['10.0.4.2/24']

fg = net.connect([f, g], capacity == mbps(100))
fg[f].ip.addrs = ['10.0.5.1/24']
fg[g].ip.addrs = ['10.0.5.2/24']

gh = net.connect([g, h], capacity == mbps(10), latency == ms(5))
gh[g].ip.addrs = ['10.0.6.1/24']
gh[h].ip.addrs = ['10.0.6.2/24']

mx.experiment(net)
