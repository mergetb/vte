import mergexp as mx
from mergexp.net import capacity, addressing, ipv4
from mergexp.unit import mbps, ms

net = mx.Topology('moa-mpl-8', addressing == ipv4)

a = net.device('a')
b = net.device('b')
c = net.device('c')
d = net.device('d')
e = net.device('e')
f = net.device('f')
link = net.connect([a, b, c, d, e, f], capacity == mbps(100))

mx.experiment(net)
