import mergexp as mx
from mergexp.machine import memory
from mergexp.unit import tb

net = mx.Topology('impossible-one')
a = net.device('a', memory == tb(47))

mx.experiment(net)

