import mergexp as mx

net = mx.Topology('multi-multi')

router = net.device('router')
local = [net.device(name) for name in ['a', 'b', 'c']]
local.append(router)
lan = net.connect(local)

for i,e in enumerate(lan.endpoints, 1):
    e.ip.addrs = ['10.0.0.%d/24'%i]

svc1 = net.device('svc1')
net.connect([router, svc1])
svc1.endpoints[0].ip.addrs = ['10.0.1.2/24']

svc2 = net.device('svc2')
net.connect([router, svc2])
svc2.endpoints[0].ip.addrs = ['10.0.2.2/24']

router.endpoints[1].ip.addrs = ['10.0.1.1/24']
router.endpoints[2].ip.addrs = ['10.0.2.1/24']

mx.experiment(net)
