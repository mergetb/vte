import mergexp as mx

net = mx.Topology('uno-router')

a = net.device('a')
b = net.device('b')
c = net.device('c')

ab = net.connect([a, b])
ab[a].ip.addrs = ['10.0.0.1/24']
ab[b].ip.addrs = ['10.0.0.2/24']

bc = net.connect([b, c])
bc[b].ip.addrs = ['10.0.1.1/24']
bc[c].ip.addrs = ['10.0.1.2/24']

mx.experiment(net)
