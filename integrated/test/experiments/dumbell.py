import mergexp as mx
from mergexp.net import capacity, latency
from mergexp.unit import mbps, ms

net = mx.Topology('dumbbell')

a = [net.device(name) for name in ['a0', 'a1', 'a2']]
b = [net.device(name) for name in ['b0', 'b1']]
r = net.device('r')

anet = net.connect(a + [r], capacity == mbps(100), latency == ms(10))
anet[r].ip.addrs = ['10.0.0.1/24']
anet[a[0]].ip.addrs = ['10.0.0.10/24']
anet[a[1]].ip.addrs = ['10.0.0.11/24']
anet[a[2]].ip.addrs = ['10.0.0.12/24']

bnet = net.connect(b + [r], capacity == mbps(200), latency == ms(20))
bnet[r].ip.addrs = ['10.0.1.1/24']
bnet[b[0]].ip.addrs = ['10.0.1.10/24']
bnet[b[1]].ip.addrs = ['10.0.1.11/24']

mx.experiment(net)
