import mergexp as mx
from mergexp.machine import image
from mergexp.net import addressing, ipv4, routing, static

net = mx.Topology('two', addressing == ipv4, routing == static)
a = net.device('a', image == 'debian:11')
b = net.device('b', image == 'debian:10')
c = net.device('c', image == 'ubuntu:2004')
ab = net.connect([a, b])
bc = net.connect([b, c])
ca = net.connect([c, a])

# specify the net object as the experiment topology created by this script
mx.experiment(net)

