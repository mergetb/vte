import mergexp as mx
from mergexp.unit import mbps, ms, gb
from mergexp.net import capacity, latency, routing, static, addressing, ipv4
from mergexp.machine import memory, image

# start by creating a topology object
topo = mx.Topology('reticulating-splines', routing == static, addressing == ipv4)

# it's just python, write your own node constructors
def node(name, group):
    dev = topo.device(name, image == 'debian:10')
    dev.props['group'] = group
    return dev
    
def router(name, group):
    dev = topo.device(name, image == 'debian:11')
    dev.props['group'] = group
    return dev

# node definitions ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
alpha   = [node(name, 1) for name in ['a0', 'a1']]
bravo   = [node(name, 2) for name in ['b0']]
charlie = [node(name, 3) for name in ['c0', 'c1', 'cgw']]
routers = [router(name, 4) for name in ['r0', 'r1']]

# connectivity ~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~~
wanlink = topo.connect(routers, capacity == mbps(1000))

# connect nodes
for a in alpha:
    topo.connect([a, routers[0]], capacity == mbps(100), latency == ms(10))

for b in bravo:
    topo.connect([b, routers[1]], capacity == mbps(5), latency == ms(80))

# connect nodes in 'LAN'
topo.connect(charlie, capacity == mbps(1000), latency == ms(1))

# gateway is special
charlie[2].props['color'] = '#f00'
dmz = topo.connect(
    [charlie[2], routers[1]],
    capacity == mbps(100),
    latency == ms(10)
)

# package up to send to merge
mx.experiment(topo)

