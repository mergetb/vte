import mergexp as mx

net = mx.Topology('two')
a = net.device('a')
b = net.device('b')
ab = net.connect([a, b])

# access endpoint by link operator
ab[a].ip.addrs = ['10.0.0.1/24']
ab[b].ip.addrs = ['10.0.0.2/24']

# specify the net object as the experiment topology created by this script
mx.experiment(net)

