import mergexp as mx
from mergexp.unit import mbps, ms
from mergexp.net import capacity, latency, routing, static
from mergexp.machine import image

topo = mx.Topology("diamond", routing == static)

topo.device("n00")
topo.device("n01")
topo.device("n02")
topo.device("n03")
topo.device("n04")
topo.device("n05")

src = topo["n00"]
dest = topo["n01"]
link = topo.connect([src, dest], capacity==mbps(10), latency==ms(1))
link[src].ip.addrs = ["10.0.0.1/30"]
link[dest].ip.addrs = ["10.0.0.2/30"]
link.props['tags'] = ['n00-n01', 'n01-n00']

src = topo["n01"]
dest = topo["n02"]
link = topo.connect([src, dest], capacity==mbps(10), latency==ms(1))
link[src].ip.addrs = ["10.0.1.1/30"]
link[dest].ip.addrs = ["10.0.1.2/30"]
link.props['tags'] = ['n01-n02', 'n02-n01']

src = topo["n01"]
dest = topo["n03"]
link = topo.connect([src, dest], capacity==mbps(10), latency==ms(1))
link[src].ip.addrs = ["10.0.2.1/30"]
link[dest].ip.addrs = ["10.0.2.2/30"]
link.props['tags'] = ['n01-n03', 'n03-n01']

src = topo["n02"]
dest = topo["n04"]
link = topo.connect([src, dest], capacity==mbps(5), latency==ms(1))
link[src].ip.addrs = ["10.0.3.1/30"]
link[dest].ip.addrs = ["10.0.3.2/30"]
link.props['tags'] = ['n02-n04', 'n04-n02']

src = topo["n03"]
dest = topo["n04"]
link = topo.connect([src, dest], capacity==mbps(8), latency==ms(1))
link[src].ip.addrs = ["10.0.4.1/30"]
link[dest].ip.addrs = ["10.0.4.2/30"]
link.props['tags'] = ['n03-n04', 'n04-n03']

src = topo["n04"]
dest = topo["n05"]
link = topo.connect([src, dest], capacity==mbps(10), latency==ms(1))
link[src].ip.addrs = ["10.0.5.1/30"]
link[dest].ip.addrs = ["10.0.5.2/30"]
link.props['tags'] = ['n04-n05', 'n05-n04']

mx.experiment(topo)

