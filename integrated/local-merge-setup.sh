#!/bin/bash

#
# run me as source to get a workable mergetb cli setup
# i.e.: 
# . local-merge-setup.sh glawler
# or, if you're using the merge-internal authorization:
# . local-merge-settup.sh -i glawler
#

function usage() {
    echo "usage: local-merge-setup.sh [-i|--internal] <username>"
    echo If \"internal\" is specified, use merge portal-internal authentication
    echo If not, use mergetb.auth0.com
}

username=""
authmode=""

while [[ $# -gt 0 ]]; do
    case $1 in
        -i|--internal-auth) 
            echo using merge-internal authentication
            authmode="-a internal"
            ;; 
        -h|--help) 
            usage
            return 1
            ;;
        *) username=$1
           echo logging in as $username
           ;;
    esac
    shift
done

if [[ -z $username ]]; then 
    usage
    return 1
fi

# set workstation routes to virtual merge portal
if ip route | grep -q 2.3.4; then
  sudo ip route del 2.3.4.0/24
fi

px=$(sudo rvn ip px)
sudo ip route add 2.3.4.0/24 via $px


# setup mergetb cli
shopt -s expand_aliases
alias mergetb="mergetb --cert /tmp/portal-keygen/ca.pem"

# sign-in to merge
if [[ ! -f ~/.config/merge/$username/token ]]; then
  if [[ $passwd ]]; then
    mergetb login $authmode $username --passwd $passwd
  else
    mergetb login $authmode $username
  fi
  if [[ $? -ne 0 ]]; then
      return
  fi
fi

mergetb join
if [[ $? -ne 0 ]]; then
    return
fi

sudo ansible-playbook -i .rvn/ansible-hosts --extra-vars "user=$username" activate.yml
