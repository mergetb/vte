# Virtual Testing Environment (VTE)

This environment contains a Merge Portal and a Merge Site for integration
testing.

Make sure you have the latest debian-buster raven image at `/var/rvn/img` from
[here](https://mirror.deterlab.net/rvn/img/).


## tl;dr

Add the following to your workstation `/etc/hosts`
```
2.3.4.10  api.mergetb.net
2.3.4.100 jumpc.mergetb.io xdc.mergetb.io mergetb.io
2.3.4.200 launch.mergetb.net
```

then launch the VTE 
```shell
# MAKE SURE YOU ARE ROOT
./run.sh
```


bootstrap merge, create relize and materialize an experiment
```shell
source local-merge-setup.sh <username>
source register-site.sh <username>
mergetb new exp hi --src test/experiments/two.py
mergetb realize hi one --accept
mergetb materialize hi one
```

## Detailed Setup

```shell
sudo su

# build and deploy the topology
source env.sh
rvn build
rvn deploy
rvn pingwait proxy master worker portalcx cmdr stor0 stor1 stor2 gw ispine ileaf0 ileaf1 xspine xleaf0 xleaf1 xleaf2 xleaf3
rvn configure
rvn status

# configure the testing environment
./install-roles.sh
ansible-playbook prepare.yml
ansible-playbook -i .rvn/ansible-hosts configure-portal.yml
ansible-playbook -i .rvn/ansible-hosts configure-site.yml
```

## Ansible Prereqs

Ansible needs to be installed and `netaddr` and `jinja2` packages are necessary.

## Hitting the API

Add the following to your workstation `/etc/hosts`

```
2.3.4.10  api.mergetb.net
2.3.4.100 jumpc.mergetb.io xdc.mergetb.io mergetb.io
2.3.4.200 launch.mergetb.net
```

Open up a terminal you plan to use to talk to Merge (not sudo). This will likely
ask you for a password. The password to provide is the one associated with the
OAuth2 account you provide as the `<username>` argument.

```shell
source local-merge-setup.sh <username>
```

### Verify API access

```shell
mergetb list projects
```

## Registring the site

```shell
source register-site.sh <username>
```

Verify you see resources

```shell
mergetb list resources
```

## Creating an experiment

The following assumes you've fetched the [models repo](https://gitlab.com/mergetb/models) and placed ~/src in gopath style.

```shell
mergetb new exp hi --src ~/src/gitlab.com/mergetb/models/xp/lan3/model.py
mergetb realize hi one --accept
mergetb materialize hi one
mergetb status hi one
```

Monitor access to nodes is available through VNC, `rvn vnc n0` to see the VNC
port for `n0`.

## Attaching XDCs via Wireguard

There are some additional steps required to attach an XDC to a materialization.
Once you have launched your XDC, use the following command to make it
attachable:

```
./xdc-config.sh <project> <experiment> <xdc>
```

