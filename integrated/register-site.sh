#!/bin/bash

if [[ $# -ne 1 ]]; then
  echo "usage: register-site.sh <username>"
  return 1
fi

mergetb new site spineleaf spineleaf.mergetb.test site/xir/spineleaf.mergetb.test.json
mergetb new pool p47
mergetb pool add site p47 spineleaf
mergetb pool activate site p47 spineleaf
mergetb pool add project p47 $username
mergetb pool activate project p47 $username
mergetb cert site spineleaf /tmp/keygen/cmdr.pem
mergetb cert wg spineleaf spineleaf.mergetb.test:36000 site/keygen/wgd.pem

mergetb pool add site defaultPool spineleaf
mergetb pool activate site defaultPool spineleaf
