#!/bin/bash

#gobgp global rib del macadv 2a:89:80:ed:26:d9 0.0.0.0 etag 0 label 2020 rd 10.99.0.5:2 rt 64705:2020 encap vxlan nexthop 10.99.0.5 origin igp -a evpn
#gobgp global rib del multicast 10.99.0.5 etag 0 rd 10.99.0.5:2 rt 64705:2020 encap vxlan nexthop 10.99.0.5 origin igp -a evpn

set -e

if [[ $# != 5 ]]; then
  echo "usage: del-evpn <mac> <vni> <rd> <as> <ip>"
  exit 1
fi

mac=$1
vni=$2
rd=$3
as=$4
ip=$5

gobgp \
  global rib del \
  macadv $mac 0.0.0.0 \
  etag 0 \
  label $vni \
  rd $ip:$rd \
  rt $as:$vni \
  encap vxlan \
  nexthop $ip \
  origin igp \
  -a evpn

gobgp \
  global rib del \
  multicast $ip \
  etag 0 \
  rd $ip:$rd \
  rt $as:$vni \
  encap vxlan \
  nexthop $ip \
  origin igp \
  -a evpn
