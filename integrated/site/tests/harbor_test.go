package test

import (
	"testing"
	"time"

	"gitlab.com/mergetb/tech/cogs/pkg"
)

// TestHarbor test harbor functionality
func TestHarbor(t *testing.T) {

	testTask("tasks/harbor-infra-links-up.yml", t)

	// need to give time to get state up before cog loads sledapi
	t.Logf("Sleeping 10 seconds")
	time.Sleep(10 * time.Second)

	// test loading some nodes into sledpi
	testTask("tasks/harbor-sled-up.yml", t)

	t.Log("harbor materialization finished")
	return
}

func testTask(specfile string, t *testing.T) {

	task, err := cogs.ReadTask(specfile)
	if err != nil {
		t.Fatal(err)
	}

	err = cogs.LaunchTask(task)
	if err != nil {
		t.Fatal(err)
	}

	err = task.Wait()
	if err != nil {
		t.Fatal(err)
	}

	failures := task.Failed()
	for _, x := range failures {
		t.Errorf("Error: %s/%s: %s", x.Kind, x.Action, *x.Error)
	}
}
