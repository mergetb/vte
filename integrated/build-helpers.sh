#!/bin/bash

RED="\e[31m"
BLUE="\e[34m"
GREEN="\e[32m"
CYAN="\e[36m"
NORMAL="\e[39m"
BLINK="\e[5m"
RESET="\e[0m"

blue() { 
  echo -e "$BLUE$1$NORMAL" 
}
green() { 
  echo -e "$GREEN$1$NORMAL" 
}
cyan() { 
  echo -e "$CYAN$1$NORMAL" 
}
stage() {
  echo -e "$BLUE$1$BLINK 🔨$RESET"
}

function parse_yaml {
   local prefix=$2
   local s='[[:space:]]*' w='[a-zA-Z0-9_]*' fs=$(echo @|tr @ '\034')
   sed -ne "s|^\($s\):|\1|" \
        -e "s|^\($s\)\($w\)$s:$s[\"']\(.*\)[\"']$s\$|\1$fs\2$fs\3|p" \
        -e "s|^\($s\)\($w\)$s:$s\(.*\)$s\$|\1$fs\2$fs\3|p"  $1 |
   awk -F$fs '{
      indent = length($1)/2;
      vname[indent] = $2;
      for (i in vname) {if (i > indent) {delete vname[i]}}
      if (length($3) > 0) {
         vn=""; for (i=0; i<indent; i++) {vn=(vn)(vname[i])("_")}
         printf("export %s%s%s=${%s%s%s:-%s}\n", "'$prefix'",vn, $2, "'$prefix'",vn, $2, $3);
      }
   }'
}

function check_sanity {

  if [[ $UID -ne 0 ]]; then
    echo "must be root"
    exit 1
  fi

}

function check_file {

  if [[ ! -x $1 ]]; then
    echo -e "$RED$1$RESET is not a thing, fix your config.yml or build the source!"
    exit 1
  fi

}

function generate_config {
  config=$1
  if [[ ! -f "$config" ]]; then
    cat <<EOF > $config
# update with your own containers
portal:
  registry: quay.io
  org: mergetb
  tag: latest

# uncomment and fill in to use custom packages
#pkg:
#  rex:
#    org: mergetb
#    repo: tech/cogs
#    ref: my-branch
#    path: build/mergetb-rex_0.7.0-1_amd64.deb
#    job: build
#  driver:
#    org: mergetb
#    repo: tech/cogs
#    ref: my-branch
#    path: build/mergetb-driver_0.7.0-1_amd64.deb
#    job: build
EOF
  fi
}
