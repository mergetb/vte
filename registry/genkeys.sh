#!/bin/bash

set -e

# get cfssl if we don't already have it
if [[ ! -f cfssl ]]; then
  curl -o cfssl -L https://pkg.cfssl.org/R1.2/cfssl_linux-amd64
  chmod +x cfssl
fi

if [[ ! -f cfssljson ]]; then
  curl -o cfssljson -L https://pkg.cfssl.org/R1.2/cfssljson_linux-amd64
  chmod +x cfssljson
fi

# generate certificate authority


cat > ca-csr.json <<EOF
{
  "CN": "mergetb",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "mergetb",
      "OU": "CA"
    }
  ]
}
EOF

if [[ ! -f certs/ca.pem ]]; then
  echo "generating ca"
  ./cfssl gencert -initca ca-csr.json | ./cfssljson -bare ca
  mkdir -p certs
  mv *.pem certs/
fi

# generate registry cert

cat > config.json <<EOF
{
  "signing": {
    "default": {
      "expiry": "4700h"
    },
    "profiles": {
      "mergetb": {
        "usages": ["signing", "key encipherment", "server auth", "client auth"],
        "expiry": "4700h"
      }
    }
  }
}
EOF

cat > reg-csr.json <<EOF
{
  "CN": "mergetb",
  "key": {
    "algo": "rsa",
    "size": 2048
  },
  "names": [
    {
      "C": "US",
      "L": "Marina del Rey",
      "ST": "California",
      "O": "Mergetb",
      "OU": "mergetb registry"
    }
  ]
}
EOF

if [[ ! -f certs/reg.pem ]]; then

    echo "generating reg cert"
    ./cfssl gencert                                      \
      -ca=certs/ca.pem                                   \
      -ca-key=certs/ca-key.pem                           \
      -config=config.json                                \
      -hostname=localhost,registry,registry.local        \
      -profile=mergetb                                   \
      reg-csr.json                                       \
      | ./cfssljson -bare reg

    mv *.pem certs/
fi


